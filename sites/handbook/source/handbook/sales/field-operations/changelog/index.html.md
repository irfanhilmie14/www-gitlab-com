---
layout: handbook-page-toc
title: "Field Operations Changelog"
description: "A running log of Field Operations changes (Sales Ops, CS Ops, Channel Ops & Deal Desk) organized by quarter and further by bi-weekly releases."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

The Field Operations Changelog keeps a running log of Field Ops changes related to Sales Ops, CS Ops, Channel Ops & Deal Desk. The log is organized by fiscal year/quarter and sub-organized by the bi-weekly Field Ops release milestone/epic. It was started in Q3-FY22 and is organized by newest updates at the top. Any entries that were included in a Field Ops Release are **bolded**.

To learn more about Field Ops releases, see the [Field Ops Release Schedule handbook page](/handbook/sales/field-operations/release-schedule/). 

## Q3-FY22 

### 2021-09-27 Release ([epic](https://gitlab.com/groups/gitlab-com/sales-team/field-operations/-/epics/62))

**Deal Desk: [Initial Term Automation for Amend Subscription Quotes](https://gitlab.com/gitlab-com/sales-team/field-operations/systems/-/issues/1539)**

**Deal Desk: [Approval Matrix Update: <12 Month Subscriptions](https://gitlab.com/gitlab-com/Finance-Division/finance/-/issues/4408)**

**Deal Desk: [Quote Layout Update: Subscription Name and Type](https://gitlab.com/gitlab-com/sales-team/field-operations/systems/-/issues/1927)**

**SOPS: [Fortune Ranking automated in SFDC](https://gitlab.com/gitlab-com/sales-team/field-operations/systems/-/issues/1935#note_680945815)**

**SOPS: [Enterprise ISR Prioritization & Opportunity Tiering](https://gitlab.com/gitlab-com/sales-team/field-operations/sales-operations/-/issues/2240)**

**SOPS: [Hide shipping address field on Account Object](https://gitlab.com/gitlab-com/sales-team/field-operations/systems/-/issues/1906)**

**FOPS: [Partner Operations (Channel + Alliances) Chatter Alias](https://gitlab.com/gitlab-com/sales-team/field-operations/systems/-/issues/2002)**

**CSOPS: [Gainsight Success Plan/Account Plan UI Upgrades](https://gitlab.com/gitlab-com/sales-team/field-operations/customer-success-operations/-/issues/568)**

### 2021-09-13 Release ([epic](https://gitlab.com/groups/gitlab-com/sales-team/field-operations/-/epics/61))

**SOPS: [Publicly Accessible Chorus Recordings](https://gitlab.com/gitlab-com/gl-security/security-operations/sirt/operations/-/issues/1560)**

**SOPS: [Account Standardization for Merging Accounts in SFDC](https://gitlab.com/gitlab-com/sales-team/field-operations/customer-success-operations/-/issues/452)**

**SOPS: [New process for US PubSec to select preferred co-sell partners for opps](https://gitlab.com/gitlab-com/channel/channels/-/issues/668)**

CSOPS: [Gainsight Dashboard filtering: added TAM Manager as filter](https://gitlab.com/gitlab-com/sales-team/field-operations/customer-success-operations/-/issues/445)

CSOPS: [At-Risk Customer Dashboard](https://gitlab.gainsightcloud.com/v1/ui/home#/8e0f5cb6-c8e1-4b06-8cd2-92af72d76615), [issue](https://gitlab.com/groups/gitlab-com/-/epics/1462) and [handbook page](/handbook/customer-success/tam/renewals/#tracking-account-health-and-risks)

CSOPS: [Gainsight: License Utilization calculations now use billable_user_count](https://gitlab.com/gitlab-com/sales-team/field-operations/customer-success-operations/-/issues/400) and [handbook](/handbook/customer-success/product-usage-data/license-utilization/)

Customer Programs: [Updated digital onboarding program and TAM playbook to include links to Docs](https://gitlab.com/gitlab-com/customer-success/tam/-/issues/470)

Customer Programs: [Launched Customer Newsletter program highlighting 14.2 release](https://gitlab.com/groups/gitlab-com/sales-team/field-operations/-/epics/34)

Customer Programs: [Synced Program emails to C360 section in Gainsight](https://gitlab.com/gitlab-com/sales-team/field-operations/customer-success-operations/-/issues/398)

Customer Programs: [Launched Customer Webinar Invite - Advanced CI/CD with largest attendance to date](https://gitlab.com/gitlab-com/sales-team/field-operations/customer-success-operations/-/issues/296)


### 2021-08-30 Release ([epic](https://gitlab.com/groups/gitlab-com/sales-team/field-operations/-/epics/60))

**Channel Ops: [GCP Marketplace Private Offer Portal Migration Complete](https://gitlab.com/gitlab-com/sales-team/field-operations/enablement/-/issues/960#note_658162138)**

**SOPS: [Salesforce: Command Plan Button - Open in New Tab](https://gitlab.com/gitlab-com/sales/-/issues/410)**

**SOPS: [LinkedIn Developer Count](https://gitlab.com/gitlab-com/sales-team/field-operations/systems/-/issues/1924)**

### 2021-08-02 Release

[Launch of Field Operations Changelog](https://gitlab.com/gitlab-com/sales-team/field-operations/enablement/-/issues/853)

[Partner Program and System Changes](https://gitlab.com/groups/gitlab-com/sales-team/field-operations/-/epics/49)

[FY22 Billing and Subscription Management Experience Improvements (Project Super Sonics) - Sales Order Processing Handbook Page Updates](/handbook/sales/field-operations/order-processing/#supersonics-billing-and-subscription-management-experience)

[Deal Desk Smart Templates](https://gitlab.com/groups/gitlab-com/sales-team/field-operations/-/epics/32)

[Expanded Criteria for Required Contact Role "GitLab Admin" as part of CLOSED WON for Enterprise/Commercial](https://gitlab.com/gitlab-com/sales-team/field-operations/systems/-/issues/1831#note_637952055)

[Sales Ops FY22 Q2 Account Moves](https://gitlab.com/gitlab-com/sales-team/field-operations/sales-operations/-/issues/2347)


